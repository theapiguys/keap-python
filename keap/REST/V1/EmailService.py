from keap.REST.V1.mixins import CreateMixin, DeleteMixin, ListMixin, RetrieveMixin


class EmailService(CreateMixin, DeleteMixin, ListMixin, RetrieveMixin):
    api_url = "emails"

    def __init__(self, keap):
        super().__init__(keap)

    def create_email(self, **kwargs):
        self._post(f"{self.api_url}", data=kwargs)

    def list_emails(self, contact_id: int = None, email: str = None, limit: int = None, offset: int = None, ordered: bool = None, since_sent_date: str = None, until_sent_date: str = None):
        params = locals()
        return self._get(f"{self.api_url}", params=params)

    def send_email(self, **kwargs):
        self._post(f"{self.api_url}", data=kwargs)

    def sync_email_records(self, **kwargs):
        self._post(f"{self.api_url}/sync", data=kwargs)

    def unsync_email_records(self, **kwargs):
        self._post(f"{self.api_url}/unsync", data=kwargs)

    def update_email_address(self, email: str, opted_in: bool = True, reason: str = "Opted in via API"):
        self._put(f"emailAddresses/{email}", data={'opted_in': opted_in, 'reason': reason})

    def list_emails_by_contact_id(self, contact_id: int):
        params = locals()
        return self._get(f"{self.api_url}", params=params)

    def get_email(self, id:int):
        return self._get(f"{self.api_url}/{id}")
